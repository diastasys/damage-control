﻿using System;
using System.Windows.Forms;
using Microsoft.Dynamics.Framework.UI.Extensibility;

namespace Diastasys.Navins.VehicleDamage
{

    [ControlAddInExport("Diastasys.Navins.VehicleDamage")]
    public interface IAdvancedExtensibilityControl
    {

       // event ControlAddInEventHandler ControlAddIn;

        [ApplicationVisible]
        event MethodInvoker AddInReady;

       
        [ApplicationVisible]
        event MethodInvoker OnReadyToReceiveControlDefinition;

        [ApplicationVisible]
        event MethodInvoker OnControlRendered;

        [ApplicationVisible]
        event DataEventHandler OnChangeData;

        [ApplicationVisible]
        event DataEventHandler OnAdminData;

        [ApplicationVisible]
        event DataEventHandler OnControlOpenPage;

        [ApplicationVisible]
        event JavaScriptExecutedEventHandler OnJavaScriptCodeExecuted;

        [ApplicationVisible]
        void SendControlDefinitionToJavaScript(string html, string css);

        [ApplicationVisible]
        void SendDataToControl(VehicleDamageMatrix data);       

        [ApplicationVisible]
        void SendAdminDataToControl(AdminDamageMatrix data);

        [ApplicationVisible]
        void ExecuteJavaScriptCode(string id, string code);

        [ApplicationVisible]
        void UpdateCss(string css);

        [ApplicationVisible]
        void ApplyStyle(string selector, string value);

        [ApplicationVisible]
        void SetVehicleType(int type);

        [ApplicationVisible]
        void SetAdminMode(bool type);

        [ApplicationVisible]
        void SetLabelValue(string id, string str);

        [ApplicationVisible]
        void SetDamagePoint(int id, string str, bool state);

        [ApplicationVisible]
        void ClearPoints();

        [ApplicationVisible]
        void SetReadOnly();


        event ControlAddInEventHandler OnControlAddIn;

    }



}

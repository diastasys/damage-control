﻿// To write your custom code, use this file

function CreateControl() {
    // This function is called when the extensibility framework instantiates the control for the
    // first time. It is not necessary to do anything here if you will send the control definition
    // HTML code to the control using the SendControlDefinitionToJavaScript function.
    // Put your HTML code into the controlSourceHtml variable. This HTML code will be inserted into
    // the myControl, which is a <div/> control with the ID #myControl.
    
    var controlSourceHtml = "";

    controlSourceHtml += "<div class='layout clear'><div class='damage-list clear'>";
  
    var no = 0;
    for (var i = 0; i < 10; i++) {
        controlSourceHtml += "<ul>";
        for (var j = 1 ; j < 21; j++) {
            no = no + 1;
            controlSourceHtml += "<li><input  type='checkbox'  name='damage[]'  id='s" + rId + no + "'><label id='l" + rId + no + "' for='s" + rId + no + "'>" + no + "</label></li>";
        }
        controlSourceHtml += "</ul>";
    }
   
    controlSourceHtml += "</div></div>";


    return controlSourceHtml;
}

function DataReceivedFromCAL(data) {
    // This function is called whenever C//AL sends data to control. Always think SourceExpr. This
    // function is called when C/AL changes the SourceExpr value.
    // To send data to C/AL, call the SendDataToCAL function.
  
    for (var i = 0; i < data.DamageMatrices.length; i++) {
        $('#l' + rId + data.DamageMatrices[i].Position).text(data.DamageMatrices[i].Id);
        $('#l' + rId + data.DamageMatrices[i].Position).show();
        $('#s' + rId + data.DamageMatrices[i].Position).parent().addClass("show");
        $('#s' + rId + data.DamageMatrices[i].Position).removeAttr("disabled");
        if (data.DamageMatrices[i].State) {
            $('#s' + rId + data.DamageMatrices[i].Position).parent().addClass("selected");
            $('#s' + rId + data.DamageMatrices[i].Position).prop('checked', true);
        }
    }
}

function DataReceivedFromAdminCAL(data) {
    // This function is called whenever C//AL sends data to control. Always think SourceExpr. This
    // function is called when C/AL changes the SourceExpr value.
    // To send data to C/AL, call the SendDataToCAL function.

    for (var i = 0; i < data.DamageMatrices.length; i++) {
        $('#l' + rId + data.DamageMatrices[i].Position).text(data.DamageMatrices[i].Id);
        $('#s' + rId + data.DamageMatrices[i].Position).parent().addClass("selected");
    }

}


function customCheckbox(checkboxName) {
    var checkBox = $('input[name="' + checkboxName + '"]');
    $(checkBox).each(function () {
        $(this).wrap("<span class='custom-checkbox'></span>");
        if ($(this).is(':checked')) {
            $(this).parent().addClass("selected");
        }
    });

    $(checkBox).click(function () {
        if(readOnly === false)
        $(this).parent().toggleClass("selected");
    });
}
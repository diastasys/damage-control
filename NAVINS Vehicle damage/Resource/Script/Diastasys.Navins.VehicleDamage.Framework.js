﻿!Microsoft && (function () { window.Microsoft = { Dynamics: { NAV: { InvokeExtensibilityMethod: function () { }, GetImageResource: function () { } } } }; })();

(/* Capturing the errors trick */function () {
    window.__controlAddInError__NAV = window.__controlAddInError;
    window.__controlAddInError = function (e) {
        debugger;
        console.log("Unhandled error has occurred: '" + e.message + "' - Stack: " + e.stack);
        window.__controlAddInError__NAV(e);
    };
})();


var navContainer;
var myControl;
var myVehicle;
var adminmode = false;
var matrixcells = 200;
var rId;
var readOnly = false;
var initialize = false;


$(document).ready(function () {
    if (initialize) {
        return;
    }

    InitializeControl();

    initialize = true;

    

    $('.damage-list input[type=checkbox]').change(function () {

        if (readOnly) {
            return;
        }

        var damage = new Damage($("label[for='" + $(this).attr('id') + "']").text(), $(this).attr('id'), this.checked);

        if (adminmode) {
            OpenPage(damage);
        } else {
            SendDataToCAL(damage);
        }

    });

    customCheckbox("damage[]");

    for (var k = 0; k < matrixcells + 1; k++) {
        $('#s' + rId + k).parent().addClass("hide");
        $('#s' + rId + k).attr("disabled", true);;
        $('#l' + rId + k).hide();
    }


});

Damage = function (id, position,state) {

    this.Id = id;
    this.Position = position.replace(/\D/g, '');
    this.State = state;

};

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function InitializeControl() {
    /// <summary>Do not call this function from JavaScript. This function is called when the control begins rendering.</summary>
     rId = randomString(2, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

    
     var ctrl2 = "myControl" + rId;
     var ctrl1 = "controlAddIn" + rId;
     
    navContainer = $('#controlAddIn');
    //navContainer = $('#' + ctrl1);
    navContainer.append('<div id="' + ctrl2 +'">' + CreateControl() + '</div>');
    //myControl = $('#myControl');
    myControl = $('#'+ ctrl2);
   

    RaiseCALEvent('OnReadyToReceiveControlDefinition', null);
}

function SendControlDefinitionToJavaScript(html, css) {
    /// <summary>Do not call this function from JavaScript. This function is called when NAV sends an additional HTML and CSS code to render in the control, as a response to the OnReadyToReceiveControlDefinition event.</summary>
    /// <param name="html">HTML code to render in the control.</param>
    /// <param name="css">CSS code to render in the control.</param>
    myControl.append(html);
    UpdateCss(css);
    RaiseCALEvent('OnControlRendered');
}

function UpdateCss(css) {
    /// <summary>Updates CSS on the page during run-time.</summary>
    /// <param name="css">Additional CSS to insert in the page.</param>
    $('body').append('<style type="text/css">' + css + '</style>');
}

function SendDataToControl(data) {
    /// <summary>Do not call this function from JavaScript. This function is called when NAV is passing data (SourceExpr) to JavaScript. This control calls the DataReceivedFromCAL function in your custom Control.js file.</summary>
    /// <param name="data">Data received from C/AL.</param>
    DataReceivedFromCAL(data);
}

function SendAdminDataToControl(data) {
    /// <summary>Do not call this function from JavaScript. This function is called when NAV is passing data (SourceExpr) to JavaScript. This control calls the DataReceivedFromCAL function in your custom Control.js file.</summary>
    /// <param name="data">Data received from C/AL.</param>
    DataReceivedFromAdminCAL(data);
}

function RaiseCALEvent(eventName, args) {
    /// <summary>Raises an event trigger in C/AL code.</summary>
    /// <param name="eventName">Name of the C/AL event trigger. The event must belong to the control and be declared in the IAdvancedExtensibilityControl interface.</param>
    /// <param name="args">Any event trigger parameters to pass to C/AL. This parameter is always of the array type, so you must enclose it in [].</param>
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod(eventName, args);
}

function SendDataToCAL(data) {
    /// <summary>Sends data to C/AL when C/AL needs to be notified of a change of the data contained in the control. Think SourceExpr here. Call this function whenever the value on the variable bound to SourceExpr in C/AL needs to be updated.</summary>
    if (readOnly === false)
       RaiseCALEvent('OnChangeData', [data]);
}

function ExecuteJavaScriptCode(id, code) {
    /// <summary>Do not call this function from JavaScript. This function is called when NAV needs to execute some ad-hoc JavaScript code.</summary>
    /// <param name="id">An identifier sent from C/AL, that will be returned back to C/AL when execution completes. This allows C/AL to know which specific custom JavaScript code was executed.</param>
    /// <param name="code">JavaScript code passed on from C/AL to execute in the control.</param>
    var f = new Function(code);

    RaiseCALEvent('OnJavaScriptCodeExecuted', [id, f()]);
}


function OnControlAddIn(index, data) {
    alert(data);
}

function SetVehicleType(type) {
    /// <summary>Updates vehicle image on the page during run-time .</summary>
    /// <param name="image">image type to update on the page.</param>

    var image;

    switch (type) {
        case 0:
            image = "0.png"; break;
        case 1:
            image = "1.png";break;
        case 2:
            image = "2.png";break;
        case 3:
            image = "3.png"; break;
        case 4:
            image = "4.png"; break;
        case 5:
            image = "5.png"; break;
        case 6:
            image = "6.png"; break;
        default:
            image = "0.png";
    }
   
    var imageUrl = Microsoft.Dynamics.NAV.GetImageResource(image);
  //  var controlSourceHtml = ".damage-list {background: url('" + imageUrl + "') no-repeat center;width: 760px;height:430px;}";
    var controlSourceHtml = ".damage-list {background: url('" + imageUrl + "') no-repeat center;}";
    $('body').append('<style type="text/css">' + controlSourceHtml + '</style>');
}

function SetAdminMode(type) {
    /// <summary>Set Administrator mode design.</summary>
    /// <param name="image">image type to update on the page.</param>
    adminmode = type;

    if (adminmode & (readOnly === false)) {
        for (var k = 0; k < matrixcells + 1; k++) {
            $('#s' + rId + k).parent().addClass("show");
            $('#s' + rId + k).removeAttr("disabled");
            $('#l' + rId + k).show();
        }
    } 

}

function OpenPage(data) {
    /// <summary>Sends data to C/AL when C/AL needs to be notified to open input page .</summary>
    RaiseCALEvent('OnControlOpenPage', [data]);
}

function SetLabelValue(id, str) {
    /// <summary>This function is called when NAV sends to change the text of the label control.</summary>
    /// <param name="Id">id of the label the control.</param>
    /// <param name="str">the text of the  control.</param>
   
    $("label[for='s" + rId + id + "']").text(str);

}

function SetDamagePoint(id, str, state) {
    /// <summary>This function is called when NAV sends to change the state of the check box control.</summary>
    /// <param name="Id">id of the checkbox control.</param>
    /// <param name="state">the state control.</param>
   
        if (state) {
            $('#s' + rId + id).parent().addClass("selected");
            $('#s' + rId + id).prop('checked', true);
        } else {
            $('#s' + rId + id).parent().removeClass("selected");
            $('#s' + rId + id).prop('checked', false);
        }

       if (str !== '' )
           $("label[for='s" + rId + id + "']").text(str);
   

}

function ClearPoints() {
    
    for (var k = 0; k < matrixcells + 1; k++) {
        $('#s' + rId + k).parent().addClass("show");
        $('#s' + rId + k).parent().removeClass("selected");
        $('#s' + rId + k).removeAttr("disabled");
        $("label[for='s" + rId + (k + 1) + "']").text((k + 1));
        $('#l' + rId + k).show();
       
    }
  
}

function SetReadOnly() {
    readOnly = true;
        for (var k = 0; k < matrixcells + 1; k++) {
            $('#s' + rId + k).attr("disabled", true);;
        }
}



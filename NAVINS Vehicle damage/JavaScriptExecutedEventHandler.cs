﻿using Microsoft.Dynamics.Framework.UI.Extensibility;

namespace Diastasys.Navins.VehicleDamage
{
    public delegate void JavaScriptExecutedEventHandler(string id, object result);
}

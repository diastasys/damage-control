﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Diastasys.Navins.VehicleDamage
{

    [DataContract, Serializable]
    public class DamageMatrix
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int Position { get; set; }

        [DataMember]
        public bool State { get; set; }

    }


    [DataContract, Serializable] 
    public class AdminDamageMatrix
    {

       
       //AdminDamageMatrix() 
       // {
       //     DamageMatrices = new List<DamageMatrix>();
       //     Messages = new List<string>();
       // }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public IList<DamageMatrix> DamageMatrices { get; set; }

        [DataMember]
        public IList<string> Messages { get; set; }

    }

    [DataContract, Serializable]
    public class VehicleDamageMatrix
    {
        //VehicleDamageMatrix()
        //{
        //    DamageMatrices = new List<DamageMatrix>();
        //    //DamageMatrices[1].Id;
        //    DamageMatrices.Insert();
        //}

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public IList<DamageMatrix> DamageMatrices { get; set; }


    }
}
